package com.example.demounittest.controller;

import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

// import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
// import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(JamkrindoController.class)
@SpringJUnitConfig
public class JamkrindoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private HttpServletRequest httpServletRequest;

    // @BeforeEach
    // public void setUp() {
    //     MockitoAnnotations.initMocks(this);
    // }

    @Test
    public void testGreetingsWithValidName() throws Exception {
        // Arrange
        String name = "Alam";
        when(httpServletRequest.getParameter("name")).thenReturn(name);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders
                .get("/jamkrindo/greetings")
                .param("name", name)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Hello Sobat Jamkrindo Alam."));
    }

    @Test
    public void testGreetingsWithMissingName() throws Exception {
        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders
                .get("/jamkrindo/greetings")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testGreetingsWithInvalidName() throws Exception {
        // Arrange
        String invalidName = "!@#$%^";
        when(httpServletRequest.getParameter("name")).thenReturn(invalidName);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders
                .get("/jamkrindo/greetings")
                .param("name", invalidName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Hello Sobat Jamkrindo !@#$%^."));
    }

     @Test
    public void testCreateUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/jamkrindo/create")
                .param("name", "Wibowo")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().string("User created: Wibowo"));
    }

    @Test
    public void testGetUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/jamkrindo/user/123")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("User details for ID: 123"));
    }
 
    @Test
    public void testUpdateUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/jamkrindo/user/123")
                .param("name", "UpdatedName")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("User updated: UpdatedName"));
    }

    @Test
    public void testDeleteUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/jamkrindo/user/123")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("User deleted for ID: 123"));
    }

}
